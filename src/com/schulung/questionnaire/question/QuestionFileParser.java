package com.schulung.questionnaire.question;

import com.schulung.questionnaire.FileProvider.FileProvider;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class QuestionFileParser {

    private static final String QUESTION_PREFIX = "?";

    public static Map<String, List<String>> parseQuestionFile(final String filePath) {
        var groupedQuestions = new HashMap<String, List<String >>();
        AtomicReference<String> currentQuestion = new AtomicReference<>("");
        var lines = FileProvider.readFile(Path.of(filePath));
        lines.forEach(line -> {
            if (line.startsWith(QUESTION_PREFIX)) {
                currentQuestion.set(line.substring(1));
                groupedQuestions.put(currentQuestion.get(), new ArrayList<>());
            } else {
                groupedQuestions.get(currentQuestion.get()).add(line);
            }
        });
        return groupedQuestions;
    }

    public static Map<String, List<String>> groupQuestionAndOptions(final List<String> lines) {
        return null;
    }
}
