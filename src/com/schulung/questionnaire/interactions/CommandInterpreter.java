package com.schulung.questionnaire.interactions;

import com.schulung.questionnaire.form.Form;

public class CommandInterpreter {

  public CommandInterpreter() {}

  public void interpret(final String option, final Form form) {
    switch (option.toUpperCase()) {
      case "S":
        Interactions.showScore(form.getQuestions());
      default:
        Interactions.answerQuestion(option, form) ;
    }
  }
}
