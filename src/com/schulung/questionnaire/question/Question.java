package com.schulung.questionnaire.question;

import com.schulung.questionnaire.answer.Option;

import java.util.List;

public class Question {
    private int id;
    private String text;
    private List<Option> options;
    private boolean isAnswered;
    private Option selectedOption;

    public Question(final int id, final String text) {
        this.id = id;
        this.text = text;
    }

    public void setAnswered(boolean answered) {
        isAnswered = answered;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public List<Option> getOptions() {
        return this.options;
    }

    public void answerQuestion(final Option selectedOption) {
        if (selectedOption != null) {
            this.selectedOption = selectedOption;
            this.isAnswered = true;
        }
    }

    public String toString() {
        var questionAnswerString = new StringBuilder();

        questionAnswerString.append(text).append("\n");
        if (options != null) {
            options.forEach(option -> {
                questionAnswerString.append(buildQuestionPrefix(option)).append("\t").append(buildOptionId(id, option.getId())).append(")").append("\t").append(option.getValue()).append("\n");
            });
        }
        return questionAnswerString.toString();
    }

    private String buildOptionId(final int questionId, final int optionId) {
        return new StringBuilder().append(questionId).append(".").append(optionId).toString();
    }

    private String buildQuestionPrefix(final Option option) {
        return isAnswered && option.getId() == selectedOption.getId() ? "*" : "";
    }

    public void setSelectedOption(Option selectedOption) {
        this.selectedOption = selectedOption;
    }
}
