package com.schulung.questionnaire.answer;

import java.util.Optional;

public class OptionBuilder {

    public static Optional<Option> buildOption(final int id, final String value) {
        if ("".equals(value)) {
            return Optional.empty();
        }
        return value.startsWith("*") ? Optional.of(new Option(id, value.substring(1), true)) : Optional.of(new Option(id, value, false));
    }
}
