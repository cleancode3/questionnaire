package com.schulung.questionnaire.question;

import com.schulung.questionnaire.answer.Option;
import com.schulung.questionnaire.answer.OptionBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class QuestionBuilder {

    public static Question createQuestion(final int id, final String questionText, final List<String> options) {
        var question = new Question(id, questionText);
        var optionsList = new ArrayList<Option>();
        AtomicInteger index = new AtomicInteger(1);
        options.forEach(optionValue -> {
            var option = OptionBuilder.buildOption(index.get(), optionValue);
            if (option.isPresent()) {
                optionsList.add(option.get());
                index.getAndIncrement();
            }
        });
        question.setOptions(optionsList);
        return question;
    }
}
