package com.schulung.questionnaire.form;

import com.schulung.questionnaire.question.Question;

import java.util.ArrayList;
import java.util.List;

public class Form {
    private List<Question> questions;

    public Form() {
        this.questions = new ArrayList<>();
    }

    public void setQuestions(List<Question> questions) {
         this.questions = questions;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}
