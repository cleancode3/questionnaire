package com.schulung.questionnaire.FileProvider;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

public class FileProvider {

    public static List<String> readFile(final Path fileName) {
        try {
            return Files.readAllLines(fileName);
        } catch (Exception e) {
            System.out.println("Error occurred");
            return Collections.EMPTY_LIST;
        }
    }
}
