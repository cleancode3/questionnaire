package com.schulung.questionnaire.answer;

public class Option {
    private int id;
    private String value;
    private boolean isCorrect;

    public Option(final int id, final String value, final boolean isCorrect) {
        this.id = id;
        this.isCorrect = isCorrect;
        this.value = value;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public String getValue() {
        return value;
    }

    public int getId() {
        return id;
    }
}
