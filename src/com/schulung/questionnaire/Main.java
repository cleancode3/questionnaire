package com.schulung.questionnaire;

import com.schulung.questionnaire.interactions.CommandInterpreter;
import com.schulung.questionnaire.interactions.Interactions;
import com.schulung.questionnaire.interactions.Menu;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String command= "";
        var form = Interactions.start(args);
        UI.display(form);

        Scanner scanner = new Scanner(System.in);
        while (!"E".equalsIgnoreCase(command)) {
            var menu = new Menu();
            menu.printMenu();
            command = scanner.next();
            var commandInterpreter = new CommandInterpreter();
            commandInterpreter.interpret(command, form);
            UI.display(form);
        }
    }
}
