package com.schulung.questionnaire.form;

import com.schulung.questionnaire.question.Question;

import java.util.List;

public class Score {

    private int total;
    private List<Question> questions;

    public Score(final int total) {
        this.total = total;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
