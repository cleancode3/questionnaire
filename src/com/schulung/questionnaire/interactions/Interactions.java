package com.schulung.questionnaire.interactions;

import com.schulung.questionnaire.commandLine.Commandline;
import com.schulung.questionnaire.form.Form;
import com.schulung.questionnaire.form.FormProvider;
import com.schulung.questionnaire.form.Score;
import com.schulung.questionnaire.question.Question;
import com.schulung.questionnaire.question.QuestionFileParser;
import com.schulung.questionnaire.question.QuestionProvider;

import java.util.List;

public class Interactions {

    public static Form start(final String args[]) {
        var pathToQuestioner = Commandline.getPath(args);
        var entries = QuestionFileParser.parseQuestionFile(pathToQuestioner);
        return FormProvider.createForm(entries);
    }

    public static Form answerQuestion(final String selectCommand, final Form form) {
        var question = QuestionProvider.updateQuestionFromAnswer(selectCommand, form.getQuestions());
        if (question.isEmpty()) {
           //Exception
        }
        return form;
    }

    public static Score showScore(final List<Question>questions) {
        return null;
    }
}
