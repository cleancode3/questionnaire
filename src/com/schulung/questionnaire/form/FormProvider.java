package com.schulung.questionnaire.form;

import com.schulung.questionnaire.question.QuestionBuilder;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class FormProvider {

    public static Form createForm(final Map<String, List<String>> entries) {
        var form = new Form();
        AtomicInteger index = new AtomicInteger(1);
        entries.forEach((questionText, options) -> {
            form.getQuestions().add(QuestionBuilder.createQuestion(index.get(), questionText, options));
            index.getAndIncrement();
        });
        return form;
    }
}
