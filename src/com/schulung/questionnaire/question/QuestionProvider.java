package com.schulung.questionnaire.question;

import com.schulung.questionnaire.answer.Option;

import java.util.List;
import java.util.Optional;

public class QuestionProvider {

    public static Optional<Question> updateQuestionFromAnswer(final String answer, final List<Question> questions) {
        if ("".equals(answer) || questions == null || questions.isEmpty()) {
            return Optional.empty();
        }
        Question question;
        var split= answer.split("\\.");
        try {
            var questionIndex = Integer.parseInt(split[0]) -1;
            question = getQuestionFromIndex(questions, questionIndex);
            var optionIndex = Integer.parseInt(split[1]) -1;
            var selectedOption = getOptionFromIndex(question, optionIndex);
            if (selectedOption != null) {
                question.answerQuestion(selectedOption);
            }
        } catch (Exception e) {
            return Optional.empty();
        }
        if (question == null) {
            return Optional.empty();
        }
        return Optional.of(question);
    }

    private static Question getQuestionFromIndex(final List<Question> questions, final int index) {
        return index < questions.size() ? questions.get(index) : null;
    }

    private static Option getOptionFromIndex(final Question question, final int index) {
        return index < question.getOptions().size() ? question.getOptions().get(index) : null;
    }
}
