package com.schulung.questionnaire;

import com.schulung.questionnaire.form.Form;

public class UI {
    public static void display(final Form form) {
        if (form == null || form.getQuestions().isEmpty()) {
            return;
        }
        form.getQuestions().forEach(question -> {
            System.out.println(question.toString());
        });
    }
}
